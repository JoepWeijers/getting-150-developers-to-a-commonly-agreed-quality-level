# Getting 150 developers to a commonly agreed quality level

This is the material used in the presentation Getting 150 developers to a commonly agreed quality level. It is a Maven project containing an example standalone WebApplication with a GitLab pipeline to illustrate the 

## Requirements
* Recent Maven
* Java 17

## Building and running as Java application

1. Package the project and run the unit tests: `mvn verify`
2. Run the packaged webapplication `java -jar target\software-quality-example-project-1.0.0-SNAPSHOT.jar`
3. Open a browser and browse to `http://localhost:8080/login/`

## Building and running as Docker container
1. Package the project and run the unit tests: `mvn verify`
2. Build the docker image: `docker build . -t joepweijers/example-webapp:latest`
3. Run the docker image: `docker run -it -p 8080:8080 joepweijers/example-webapp:latest`
3. Open a browser and browse to `http://localhost:8080/login/`

## GitLab CI Pipeline explanation

In the GitLab pipeline we verify the following TOPdesk quality requirements:
* Be explicit about dependencies, by running `mvn dependency:analyze -DfailOnWarning=true`
* Dependencies must converge, by running `mvn enforcer:enforce -Drules=dependencyConvergence`
* Project uses a proper Java version, by inspecting the Dockerfile: `if cat Dockerfile | grep -c -q  -E "eclipse-temurin:17\.[0-9]+\.[0-9]+(_[0-9]+)?-jre-jammy"; then echo 'Proper Java Version Used' && exit 0; else echo 'Improper Java version used' && exit 1; fi`

All jobs in the pipeline are allowed to fail, which will turn their icon yellow with an exclamation mark. While we want our developers to adhere to the quality requirements, we don't feel that not adhering should be a reason to not be able to deploy. Therefore only a warning, and not a failed pipeline.

Note that for illustration purposes I have made these explicit calls to the Maven command line interface. In our implementation we make as many checks as possible part of the build, such that warnings are already shown on developers machines. But that would make the pipeline a simple `mvn verify`, not demonstrating the concept of having different jobs checking different quality requirements.

## Material referenced in the presentation
### TimeTransformer
A tool to manipulate the time on a running Java Virtual Machine. Only to be used in code that can't be rewritten to be properly testable!!

Code: https://github.com/TOPdesk/time-transformer-agent

Example project: https://github.com/TOPdesk/time-transformer-examples

### TOPdesk Tech blog
Our technical blog, explaining challenges we run into and how we tackle them. Topics include amongst other frontend (NPM), backend (Java, Maven), software quality and public speaking: https://techblog.topdesk.com/

### TOPdesk summer internship 2023 (advertisement)
Every year TOPdesk organizes a paid summer internship for Sales, Support and Development. Hone your skills in a multi week project in a fun environment. Unfortunately the development summer internship is already full. For Sales and support speaking Dutch is required: https://careers.topdesk.com/nl/summerinternship/

## License
MIT License
