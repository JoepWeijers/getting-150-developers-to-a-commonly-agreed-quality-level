package nl.joepweijers.presentations.improper_unit_tests;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CountingDaysTest {
	class ClassUnderTest {
		long calculateDaysBetweenNowAndNextMonth(LocalDate date) {
			return ChronoUnit.DAYS.between(date, date.plus(1, ChronoUnit.MONTHS));
		}
	}
	
	@Test
	void testDaysBetweenTwoInstants() {
		LocalDate date = LocalDate.now();
		long actual = new ClassUnderTest().calculateDaysBetweenNowAndNextMonth(date);
		Assertions.assertEquals(actual, expectedDays(date));
	}
	
	long expectedDays(LocalDate date) {
		switch (date.getMonth()) {
		case FEBRUARY: return 28;
		case APRIL, JUNE, SEPTEMBER, NOVEMBER: return 30;
		default: return 31;
		}
	}
}
