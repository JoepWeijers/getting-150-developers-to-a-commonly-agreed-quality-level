package nl.joepweijers.presentations.improper_unit_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class SystemPropertiesTest {
	class ClassUnderTest {
		String createOutput(String message) {
			if ("true".equalsIgnoreCase(System.getProperty("verbose"))) {
				return "verbose: " + message;
			}
			return message;
		}
	}
	
	@Test
	void testOutputFirst() {
		String actual = new ClassUnderTest().createOutput("hello world");
		assertEquals("hello world", actual);
	}
	
	@Test
	void testOutputVerbose() {
		System.setProperty("verbose", "true");
		String actual = new ClassUnderTest().createOutput("hi world");
		assertEquals("verbose: hi world", actual);
	}
}
