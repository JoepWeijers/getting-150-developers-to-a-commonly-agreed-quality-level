package nl.joepweijers.presentations.improper_unit_tests;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FlopdeskTest {
	@Test
	void testServerIsResolvable() throws IOException {
		Assertions.assertEquals(200, connectToServer(new URL("https://www.topdesk.com")));
	}
	
	@Test
	void testServerIsUnResolvable() {
		Assertions.assertThrows(UnknownHostException.class,
				() -> connectToServer(new URL("http://www.flopdesk.com")));
	}
	
	private int connectToServer(URL url) throws IOException {
		return ((HttpURLConnection)url.openConnection()).getResponseCode();
	}
}
