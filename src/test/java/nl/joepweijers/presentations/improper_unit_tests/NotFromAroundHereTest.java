package nl.joepweijers.presentations.improper_unit_tests;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NotFromAroundHereTest {
	@Test
	void testLocalizedDate() {
		LocalDate date = LocalDate.parse("2023-05-31");
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
		Assertions.assertEquals("31 mei 2023", date.format(formatter));
	}
}
