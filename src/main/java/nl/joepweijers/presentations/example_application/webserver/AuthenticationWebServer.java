package nl.joepweijers.presentations.example_application.webserver;

import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.PASSWORD_PARAMETER;
import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.USERNAME_PARAMETER;
import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.buildLoginFormResponse;
import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.buildWelcomePage;
import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.consumePostRequestBody;
import static nl.joepweijers.presentations.example_application.webserver.HtmlUtils.sendHtmlResponse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class AuthenticationWebServer {
	private static final int PASSWORD_ATTEMPTS = 3;
	
	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(getWebServerPort()), 0);
		server.createContext("/login", new AuthenticationHandler());
		server.start();
		log("HttpServer started and listening on: " + server.getAddress());
	}
	
	private static void log(String message) {
		if (!Boolean.getBoolean("quiet")) {
			System.out.println(message);
		}
	}
	
	private static int getWebServerPort() {
		return Integer.getInteger("webserverPort", 8080);
	}
	
	private static final class AuthenticationHandler implements HttpHandler {
		private FailedLoginAttemptsManager failedLoginAttemptsManager = new FailedLoginAttemptsManager(PASSWORD_ATTEMPTS);
		
		@Override
		public void handle(HttpExchange exchange) throws IOException {
			if (!"POST".equalsIgnoreCase(exchange.getRequestMethod())) {
				sendHtmlResponse(exchange, buildLoginFormResponse(""));
				return;
			}
			
			Map<String, String> postData = consumePostRequestBody(exchange);
			if (authenticate(postData)) {
				sendHtmlResponse(exchange, buildWelcomePage(postData.get(USERNAME_PARAMETER)));
			}
			else {
				sendHtmlResponse(exchange, buildLoginFormResponse("Incorrect login"));
			}
		}
		
		private boolean authenticate(Map<String, String> postData) {
			String username = postData.get(USERNAME_PARAMETER);
			String password = postData.get(PASSWORD_PARAMETER);
			if (!failedLoginAttemptsManager.isLockedOut(username) && "admin".equals(username) && "admin".equals(password)) {
				return true;
			}
			failedLoginAttemptsManager.registerFailedLoginAttempt(username);
			return false;
		}
	}
}
