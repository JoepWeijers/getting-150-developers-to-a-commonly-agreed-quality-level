FROM eclipse-temurin:17.0.7_7-jre-jammy
RUN mkdir /opt/webapp/
COPY target/software-quality-example-project-1.0.0-SNAPSHOT.jar /opt/webapp/webapp.jar
ENTRYPOINT java -jar /opt/webapp/webapp.jar